import 'dart:async';
import 'package:controle_validade/app/Services/Auth/AuthService.dart';
import 'package:controle_validade/app/Services/Storage/StorageService.dart';
import 'package:mobx/mobx.dart';


import 'package:mobx/mobx.dart';
part 'login_controller.g.dart';

class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
    final AuthService authService =  AuthService();
    final StorageService storageService =  StorageService();

  // Future login(String email, String password) async {
  //   var thing = await authService.login(email, password);
  // }

  @observable
  String email = '';

  @action
  void setEmail(String value) => email = value;

  @observable
  String password = '';

  @action
  void setPassword(String value) => password = value;

  @observable
bool passwordVisible = false;

@action
void togglePasswordVisibility() => passwordVisible = !passwordVisible;

@observable
bool errorMensage = false;

@action
void toggleAlertMensage() => errorMensage = !errorMensage;

@observable
bool loading = false;

@observable
bool loggedIn = false;

@computed
bool get isEmailValid => email.length >= 6 && email.contains('@');

@computed
bool get isPasswordValid => password.length >= 6;

@computed
Function get loginPressed => (isEmailValid && isPasswordValid && !loading) ? login : null;


Future<void> disableButton() {
  return null;
}


Future<void> teste() async{
    var testevar = await storageService.getValue('usertoken');
    print(testevar);
}

@action
Future<void> login() async{
  loading = true;
  errorMensage = false;

  var requestResult = await authService.login(email, password);
  try{
  if(requestResult != null){
  storageService.setUserToken('usertoken', requestResult['token']);
  storageService.setUserId('userId', requestResult['id']);
  }else{
    print('deu erro aqui!');
    loading = false;
    toggleAlertMensage();

  }

  }catch (error) {
    print(error);
}


  loading = false;
  loggedIn = true;

  email = '';
  password = '';

}

@action
void logout(){
  loading = false;
}

  Future registerUser() async{
    const userForm = {
      'firstName': 'Alisson Mateus',
      'email': 'alisson_mateus96@outlook.com',
      'password': '123456'
    };

    var result = await authService.register(userForm);
  }
}


