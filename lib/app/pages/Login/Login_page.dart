import 'package:controle_validade/app/pages/Login/login_controller.dart';
import 'package:controle_validade/app/widgets/alert_dialog.dart';
import 'package:controle_validade/app/widgets/custom_icon_button.dart';
import 'package:controle_validade/app/widgets/inputField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  LoginController loginController = LoginController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff0e3452),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 50, bottom: 10),
              child: Image.asset('assets/images/calendar.png',
                  height: 150, width: 150),
            ),
            Text('Controle de Validade',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.white)),
            Padding(
              padding: EdgeInsets.only(top: 30, left: 15, right: 15),
              child: Theme(
                  data: Theme.of(context)
                      .copyWith(primaryColor: Color(0xff4169E1)),
                  child: Observer(builder: (_) {
                    return CustomTextField(
                        hint: "E-mail",
                        prefix: Icon(Icons.account_circle),
                        textInputType: TextInputType.emailAddress,
                        onChanged: loginController.setEmail,
                        enabled: true);
                  })),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15, right: 15, top: 15),
              child: Theme(
                  data: Theme.of(context)
                      .copyWith(primaryColor: Color(0xff4169E1)),
                  child: Observer(builder: (_) {
                    return CustomTextField(
                      hint: 'Senha',
                      prefix: Icon(Icons.lock),
                      obscure: !loginController.passwordVisible,
                      onChanged: loginController.setPassword,
                      enabled: true,
                      suffix: CustomIconButton(
                        radius: 32,
                        iconData: loginController.passwordVisible? Icons.visibility_off: Icons.visibility,
                        onTap: loginController.togglePasswordVisibility,
                      ),
                    );
                  })),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5, left: 25, right: 25),
              child: Observer(builder:(_){
                return loginController.errorMensage? alertbox(): emptyWidget();
              })
            ),
            Padding(
                padding: EdgeInsets.only(top: 30),
                child: Observer(builder: (_) {
                  return SizedBox(
                    height: 44,
                    child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32),
                        ),
                        child: loginController.loading
                            ? CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation(Colors.white),
                              )
                            : Text('Login', style: TextStyle(color: Colors.white),),
                        color: Color(0xff00BFFF),
                        disabledColor:
                            Color(0xffADD8E6),
                        textColor: Colors.white,
                        onPressed: loginController.loginPressed
                        ),
                  );
                })),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: RaisedButton(
                elevation: 0.0,
                color: Color(0xff0e3452),
                onPressed: () {
                  sendToRegisterPage();
                },
                child: new Text(
                  'Ainda não tem conta? Cadastre-se!',
                  style: TextStyle(color: Color(0xff00BFFF)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

    // showAlertDialog(){
    //    return showDialog(context: context, builder:(context){
    //         return AlertModal(
    //     title:'Falha ao logar',
    //     content: 'E-mail ou senha invalidos');
    //    });
    // }

    sendToRegisterPage(){
      Navigator.pushNamed(context, '/register');
    }

    alertbox(){
      return  SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 30.0,
                child: Card(
                  color: Color(0xffDC143C),
                  child: Center(
                    child: Text(
                    'Falha ao logar: E-mail ou senha invalidos', 
                    style: TextStyle(color: Colors.white),
                    ),
                  )
                    )
              );
    }

  Widget emptyWidget(){
      return Container(

      );

  }
}
