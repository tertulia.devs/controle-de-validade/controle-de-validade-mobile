import 'package:controle_validade/app/pages/Register/register_page.dart';
import 'package:controle_validade/app/Services/Auth/AuthService.dart';
import 'package:controle_validade/app/Services/Storage/StorageService.dart';
import 'package:mobx/mobx.dart';
part 'register_controller.g.dart';

class RegisterController = _RegisterControllerBase with _$RegisterController;

abstract class _RegisterControllerBase with Store {
  
  final AuthService authService =  AuthService();
  final StorageService storageService =  StorageService();

  @observable
  String userName = '';

  @observable
  String email = '';

  @observable
  String password = '';

  @observable
  String passwordConfirm = '';

  @observable
  bool loading = false;

  @observable
  bool errorMensage = false;

@action
void toggleAlertMensage() => errorMensage = !errorMensage;

  @action
  void setName(String value) => userName = value;

  @action
  void setEmail(String value) => email = value;

  @action
  void setPassword(String value) => password = value;

  @action
  void setPasswordConfirm(String value) => passwordConfirm = value;

  @computed
  bool get isNameValid => userName.length >= 3;

  @computed
  bool get isEmailValid => email.length >= 6 && email.contains('@');

  @computed
  bool get isPasswordValid => password.length >= 6;

  @computed
  bool get isConfirmedPassword{
    if(password == passwordConfirm && password.length > 0){
      return true;
    }else{
      return false;
    }
  }

  @computed
  Function get registerValid => (isNameValid && isEmailValid && isPasswordValid && isConfirmedPassword) ?  register : null;


  Future <void> register() async {
    loading = true;
    var dataForm = {
      'firstName': userName,
      'email': email,
      'password': password
    }; 
    var requestResult = await authService.register(dataForm);
    try{
      if(requestResult != null){
        storageService.setUserToken('usertoken', requestResult['token']);
        storageService.setUserId('userId', requestResult['id']);
        loading = false;
      }else{
        print('deu erro aqui!');
        loading = false;
        toggleAlertMensage();
      }
    }catch (error){
      loading = false;
      toggleAlertMensage();
    }
  }

}