import 'package:controle_validade/app/pages/Register/register_controller.dart';
import 'package:flutter/material.dart';
import 'package:controle_validade/app/widgets/inputField.dart';
import 'package:controle_validade/app/widgets/custom_icon_button.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  RegisterController registerController = RegisterController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xff0e3452),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 50, bottom: 20),
                    child: Image.asset('assets/images/calendar.png',
                        height: 100, width: 100)),
                Center(
                  child: Text(
                    'Novo cadastro',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 24),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                  child: Observer(builder: (_) {
                    return CustomTextField(
                      hint: 'Nome de Usuário',
                      prefix: Icon(Icons.account_circle),
                      obscure: false,
                      onChanged: registerController.setName,
                      enabled: true,
                      suffix: CustomIconButton(
                        radius: 32,
                        iconColor: registerController.isNameValid
                            ? Colors.green
                            : Colors.red,
                        iconData: Icons.done,
                        onTap: null,
                      ),
                    );
                  }),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.only(left: 30),
                    child: Text(
                      'Minímo de 3 caracteres',
                      style: TextStyle(color: Colors.red,  fontSize: 10),
                    ),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.all(10),
                    child: Observer(builder: (_) {
                      return CustomTextField(
                        hint: 'E-mail',
                        prefix: Icon(Icons.email),
                        obscure: false,
                        onChanged: registerController.setEmail,
                        enabled: true,
                        suffix: CustomIconButton(
                          radius: 32,
                          iconColor: registerController.isEmailValid
                              ? Colors.green
                              : Colors.red,
                          iconData: Icons.done,
                          onTap: null,
                        ),
                      );
                    })),
                Padding(
                    padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                    child: Observer(builder: (_) {
                      return CustomTextField(
                        hint: 'Senha',
                        prefix: Icon(Icons.lock),
                        obscure: true,
                        onChanged: registerController.setPassword,
                        enabled: true,
                        suffix: CustomIconButton(
                          radius: 32,
                          iconColor: registerController.isPasswordValid
                              ? Colors.green
                              : Colors.red,
                          iconData: Icons.done,
                          onTap: null,
                        ),
                      );
                    })),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.only(left: 30),
                    child: Text(
                      'Minímo de 6 caracteres',
                      style: TextStyle(color: Colors.red, fontSize: 9),
                    ),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.all(10),
                    child: Observer(builder: (_) {
                      return CustomTextField(
                        hint: 'Repetir Senha',
                        prefix: Icon(Icons.lock),
                        obscure: true,
                        onChanged: registerController.setPasswordConfirm,
                        enabled: true,
                        suffix: CustomIconButton(
                          radius: 32,
                          iconColor: registerController.isConfirmedPassword
                              ? Colors.green
                              : Colors.red,
                          iconData: Icons.done,
                          onTap: null,
                        ),
                      );
                    })),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width / 2,
                    height: 44,
                    child: Observer(builder: (_) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32),
                          ),
                          child: registerController.loading
                              ? CircularProgressIndicator(
                                  valueColor:
                                      AlwaysStoppedAnimation(Colors.white),
                                )
                              : Text(
                                  'Cadastrar',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                          color: Color(0xff0C71585),
                          disabledColor: Color(0xffD8BFD8),
                          textColor: Colors.white,
                          onPressed: registerController.registerValid);
                    }),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 5, left: 25, right: 25),
                    child: Observer(builder: (_) {
                      return registerController.errorMensage
                          ? alertbox()
                          : emptyWidget();
                    })),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: RaisedButton(
                    elevation: 0.0,
                    color: Color(0xff0e3452),
                    onPressed: () {
                      sendToLoginPage();
                    },
                    child: new Text(
                      'Já tenho conta, Voltar.',
                      style: TextStyle(color: Color(0xff00BFFF)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
    sendToLoginPage(){
      Navigator.pop(context);
    }
  Widget emptyWidget() {
    return Container();
  }

  alertbox() {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 30.0,
        child: Card(
            color: Color(0xffDC143C),
            child: Center(
              child: Text(
                'Falha ao cadastrar: Tente novamente mais tarde',
                style: TextStyle(color: Colors.white),
              ),
            )));
  }
}
