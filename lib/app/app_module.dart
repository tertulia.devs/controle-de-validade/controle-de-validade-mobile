import 'package:controle_validade/app/pages/Login/Login_page.dart';
import 'package:controle_validade/app/pages/Register/register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:controle_validade/app_widget.dart';
import 'package:controle_validade/app/Services/Auth/AuthService.dart';
import 'package:dio/dio.dart';


class AppModule extends MainModule {

  @override
  List<Bind> get binds => [
  ];

  @override
  List<ModularRouter> get routers => [
    ModularRouter('/', child: (_,__) =>Login()),
    ModularRouter('/register', child: (_,__) =>Register())
  ];

  @override
  Widget get bootstrap => AppWidget();
}