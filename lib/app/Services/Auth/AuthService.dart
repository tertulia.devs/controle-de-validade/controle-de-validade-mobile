import 'package:controle_validade/app/Settings/settings.dart';
import 'package:dio/dio.dart';
import 'dart:convert';

import 'package:flutter_modular/flutter_modular.dart';

class AuthService extends Disposable {
  Dio dio = new Dio();
  
  Settings settings = Settings();

  Future login(String email, String password) async{
    Map params = {
      'email' : email,
      'password': password
    };

    try{
    var _body = json.encode(params);

    var response = await dio.post(
      '${settings.API_URL_BASE}accounts/login/',
      data: _body,
      options: Options(
        followRedirects: false,
        
      ));
    print(response.statusCode);
    print(response.data);
    return response.data;
    
    } catch(e){
      print(e);
    }
  }

  Future register(dataForm) async {
    Map params = {
      'name': dataForm['firstName'],
      'email': dataForm['email'],
      'password': dataForm['password']
    };
    try{
      var response = await dio.post('https://controledevalidade.herokuapp.com/api/accounts/register/',
        data: params,
      );

      print(response.statusCode);
      print(response.data);

      return response.data;
      
    } catch(e){
      print(e);
    }
  }


  @override
  void dispose() {}

}