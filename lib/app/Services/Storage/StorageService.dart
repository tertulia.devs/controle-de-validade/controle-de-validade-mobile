import 'package:controle_validade/app/Settings/settings.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorageService {

Settings settings = Settings();

Future setUserToken(key, value) async{
final prefs = await SharedPreferences.getInstance();
prefs.setString(key, value);
}
Future setUserId(key, value) async{
final prefs = await SharedPreferences.getInstance();
prefs.setInt(key, value);
}

Future getValue(key) async{
final prefs = await SharedPreferences.getInstance();
var result = prefs.getString(key) ?? '';
return result;
}
Future clearValue(key) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.remove(key);
}

}