import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {

  CustomIconButton({this.radius, this.iconData, this.onTap, this.iconColor});

  final double radius;
  final IconData iconData;
  final VoidCallback onTap;
  final Color iconColor;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child : Material(
        color: Colors.transparent,
        child : InkWell(
          child : new IconTheme(
            data: new IconThemeData(color: iconColor), 
            child: Icon(iconData)),
          onTap: onTap,
        ),
      ),
    );
  }
}