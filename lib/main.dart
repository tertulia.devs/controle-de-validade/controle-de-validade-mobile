import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';


import 'app/app_module.dart';

void main() => runApp(ModularApp(module: AppModule()));
// void main(){
//   runApp(MaterialApp(
//     home: Login(),
//     theme: ThemeData(
//       primaryColor: Color(0xff075E54),
//       accentColor: Color(0xff25D366)
//     ),
//   ));
// }